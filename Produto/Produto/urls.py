from django.contrib import admin
from django.urls import include, path
from produtoApp import urls as urlsProdutos

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(urlsProdutos))
]