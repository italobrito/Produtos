from django.forms import ModelForm
from .models import Produtos

class ProdutoForm(ModelForm):
    class Meta:
        model = Produtos
        fields = ['nome', 'codigo', 'vencim', 'valorn', 'custo', 'peso']