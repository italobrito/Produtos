from django.urls import path
from .views import *

urlpatterns = [
    path('list/', produtos_list, name='home'),
    path('new/', produtos_new, name='produtos_form'),
    path('update/<int:id>/', produtos_update, name='produtos_update'),
    path("delete/<int:id>/", produtos_delete, name='produtos_delete'),
]